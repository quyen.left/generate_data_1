import express from 'express';
import typeorm  from './loader/typeorm';
import Radom from './utils/RadomRegion';
import Convert from './utils/Convert'
import {getRepository} from "typeorm";
import moment from 'moment';
import {Site} from 'gsec-entity-module';
import {Lift} from 'gsec-entity-module';
import {Building} from 'gsec-entity-module';
const app=express();

const PORT =6003;
const LIMIT=5000;
const building=2;
const lift=10;
app.get('/createsite', async(req,res)=>{
    const date=moment(new Date()).add(2,'years');
    const date1=moment(date,'YYYY-MM-DD hh:mm:ss').toDate();
    const liftRepo=getRepository(Lift);
    const siterRepo= getRepository(Site);
    const buildingRepo= getRepository(Building);
    const contractNo="SITEDEMO_";
    const site_code="SITE_CODE_";
    const site_name="SITENAME_";
    const site_address="SITE_ADDRESS_";
    const dong_qty=10;
    const car_qty=10;
    const latitude=10.79593754;
    const longitude=106.66261292;
    const name_building= "BUILDING_";
    const lift_contract='LIFT_CONTRACT_';
    for(let i=0;i<LIMIT;i++){
        const newSite= new Site();
        newSite.contract_no = contractNo +i;
        newSite.site_code=site_code +i ;
        newSite.site_name=site_name +i;
        newSite.site_address=site_address+i;
        newSite.dong_qty=dong_qty;
        newSite.car_qty=car_qty;
        newSite.latitude=await Convert(latitude,i);
        newSite.longitude=await Convert(longitude,i);
        newSite.region=Radom(1,63);
        const site1=await siterRepo.save(newSite);
        for(let l=0 ;l<building;l++){
            const newBuilding= new Building();
            newBuilding.name=site1.contract_no.concat(name_building + l);
            newBuilding.lift_qty=lift;
            newBuilding.site=site1;
            const building1=await buildingRepo.save(newBuilding);
            for(let k=0;k< Math.floor(lift/building);k++){
                const newLift=new Lift();
                newLift.site=site1;
                newLift.building=building1;
                newLift.floor_list='1,2,3';
                newLift.lift_no=k;
                newLift.latitude=site1.latitude;
                newLift.longitude=site1.longitude;
                newLift.warranty_to=date1;
                newLift.performance=100;
                newLift.type=1;
                newLift.status=1;
                newLift.model_no='LIFT_19_'+building1.id;
                newLift.lift_contract_no=lift_contract+site1.contract_no+building1.id+"_"+k;
                const lift1=await liftRepo.save(newLift);
            }
        }
    }
    res.send("insert oke!!!");
});
app.listen(PORT,async()=>{
    await typeorm();
    console.log(`Server Start At PORT ${PORT}`);
})