import { createConnection, useContainer } from 'typeorm';
import path from 'path';


export default async () => {
  // TypeOrm + TypeDI
  
  const connection = await createConnection({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: '',
    database: 'gsec3',
    entities: [path.join(__dirname, '../../node_modules/gsec-entity-module/build/entity/*.{js,ts}')],
    synchronize: false,
    bigNumberStrings: false,
    timezone: 'Z',
  });

  return connection;
};