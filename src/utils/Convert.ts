export default async (a,b)=>{
    const string=String(a).split('.');
    const array= new Array(string[0],String(Number(string[1])+b));
    return await Number(array.join('.'));
}